const jp = require('jsonpath');

export default function validateJsonPath(expr) {
    try {
        jp.parse(expr);
        return {
            "status": true
        }
    } catch(error) {
        return {
            "status": false,
            "message": error
        }
    }
}