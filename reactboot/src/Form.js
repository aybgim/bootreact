import React, { useEffect } from "react";
import validateJsonPath from "./JSONPathValidator"

export default function Form() {
    useEffect(() => {
        alpacaForm("#form", {
            schema: {
                title: "Look what Alpaca can do!",
                type: "object",
                properties: {
                    primary: {
                        type: "string",
                        title: "Primary field",
                        required: true,
                    },
                    secondary: {
                        type: "string",
                        title: "Secondary field"
                    },
                    jsonpath: {
                        type: "string",
                        title: "JSONPath"
                    }
                }
            },
            options: {
                fields: {
                    primary: {
                        type: "select",
                        dataSource: '/primary'
                    },
                    secondary: {
                        type: "select",
                        enum: []
                    },
                    jsonpath: {
                        validator: function (callback) {
                            const expr = this.getValue();
                            callback(validateJsonPath(expr))
                        }
                    }
                }
            },
            postRender: function (control) {
                const primary = control.childrenByPropertyId["primary"];
                const secondary = control.childrenByPropertyId["secondary"];
                secondary.subscribe(primary, function (primaryId) {
                    fetch("secondary/" + primaryId)
                        .then(response => response.json())
                        .then(Object.values)
                        .then(data => {
                            this.schema.enum = data;
                            this.refresh();
                        });
                });
            }
        });
    }, []);

    return <>
        <h2>Alpaca Form</h2>
        <div id="form" />
    </>;
}
